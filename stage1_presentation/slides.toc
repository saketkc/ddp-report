\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{\scshape Introduction}{1}{0}{1}
\beamer@subsectionintoc {1}{1}{Objective}{3}{0}{1}
\beamer@sectionintoc {2}{\scshape Significant Mutations}{16}{0}{2}
\beamer@subsectionintoc {2}{1}{Next Generation Sequencing}{16}{0}{2}
\beamer@subsectionintoc {2}{2}{Motivation}{31}{0}{2}
\beamer@subsectionintoc {2}{3}{Computational Methods for Driver Detection}{39}{0}{2}
\beamer@sectionintoc {3}{\scshape Viral Genome Detecion}{52}{0}{3}
\beamer@subsectionintoc {3}{1}{Workflow}{53}{0}{3}
\beamer@sectionintoc {4}{\scshape Reproducibility}{62}{0}{4}
\beamer@subsectionintoc {4}{1}{Reproducibility}{62}{0}{4}
\beamer@sectionintoc {5}{BWA v/s BWA-PSSM}{63}{0}{5}
\beamer@sectionintoc {6}{\scshape Conclusions}{66}{0}{6}
\beamer@subsectionintoc {6}{1}{Wrapping up}{66}{0}{6}
