\tikzstyle{blockred} = [rectangle, draw, fill=red!20, 
text width=5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{block} = [rectangle, draw, fill=white!20, 
text width=5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{blockgreen} = [rectangle, draw, fill=green!20, 
text width=5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex']

\chapter{Analysis of Microarray Data}
\ifpdf
    \graphicspath{{Chapter7/Chapter7Figs/PNG/}{Chapter7/Chapter7Figs/PDF/}{Chapter7/Chapter7Figs/}}
\else
    \graphicspath{{Chapter7/Chapter7Figs/EPS/}{Chapter7/Chapter7Figs/}}
\fi

\section*{Organisation}

This chapter is an introductory overview of the Microarray technology. The following sections discuss \textit{-omics} research. The next follow up section describe the data analysis workflow for pre-processing the raw data. The penultimate section describes the outcome of this pre-processing work-flow when applied to a microarray study, followed by a discussion section on the  overall recommendations for running such work-flows.

\section{Introduction}
One of the technologies that matches the scale of sequencing platforms in terms
of the data involved is Microarray technology. Microarrays
have been to study gene expression levels of thousands of genes at once. The other applications of microarray technology 
deals with gene variation analysis.


Microarray technology is one of the many techniques that emerged from the field of 'functional genomics'. 'Functional genomics' itself is a sub-field  of molecular biology that primarily focuses
on studying the dynamic aspects of 'genomics' such as the gene expression values. 'Genomics' on the other side, deals with the more \textit{static} part of genome such as determining the whole sequence of genome.




\section{-Omics Research }
With the advent of the term 'Genomics', the other \textit{-omics} terms were created, subsequently.

\subsection{Genomics}
The term 'Genomics' was coined by Dr. Thomas Roderick of the Jackson Laboratory at an international meeting on the feasibility of mapping the entire human genome, in 1986. \cite{yadav2007wholeness}
\textit{Genomics} deals with analysis of \textit{genome} sequences in order to analyse the associated function and structure. A sub-domain of this field includes \textit{functional genomics} that focuses on the \textit{functional} implications of genomics, thus studying genes and their products at the expression level. \textit{Genomics} and \textit{Genetics} are treated differently, since the latter is understood  more from the point of view of study of genes from the point of view of \textit{genetic} inheritance.

\subsection{Proteomics}
\textit{Proteomics} is the functional and structural study of proteins. The scope of insight from Genomics is limited from a biological point of view, since the focus is on the \textit{static} contents of the genome. Proteomics is a \textit{functional} approach and hence can be more relevant from a biological point of view, given the focus is on protein levels. Protein levels determine  cell physiology. Unlike genome content, the proteomic content has a dynamic nature and is constantly changing. One cell can have totally different proteome levels at different points of time, irrespective of the state of the cell(whether or not it has been \textit{diseased/affected} by external or internal factors)


\subsection{Transcriptomics}

\textit{Transcriptome} is the net total of all RNA content in a given cell type  including mRNA, tRNA, rRNA, and other non-coding RNA. It is different from exons as exons consists of DNA transcribed to RNA in any cell type, whereas transcriptome is the transcribed RNA in a \textit{specific} cell type. \textit{Transcriptomics} or expression profiling is the study of these expression levels in a given cell type. High throughput transcriptomics  is called \textit{RNA-Seq}



\section{Microarray Technology}
%\subsection{Microarrays: Experimental Design}

\subsection{Motivation}
The motivation behind performing a microarray study is to identify those set of marker
genes that could differentiate two or more conditions. In cancer studies, any 
particular insight into which set of genes are up or down regulated in the tumor tissue
as compared to the benign tissues, can lead to potential therapeutic applications where the drug could be designed to control or repress the specific gene's expression levels. Another potential output of a microarray study could be a \textit{bio-marker}, a relatively small panel of \textit{marker genes} that could be used in the place of whole microarray as a prognosis kit in order to determine the category of a particular sample(normal/disease), and hence if the whole expression profile of this unknown sample would be \textit{similar} to those known to be suffering from the disease.

\subsection{Experimental Design}
A DNA microarray is essentially a lab-on-a-chip device with spots of fragments of DNA sequences attached. These spots contain small fragments of DNA that in turn would \textit{hybridize} when exposed to a \textit{target}. This hybridization is \textit{quantifiable}  by detection of intensity of fluorescent-dye signals.  This signal intensity is proportional to the amount of sample-target bonding. A dual channel  experiment would involve quantification of this intensity for two sets of samples, say \textit{normal} and \textit{disease}, under two different fluorescent signals. For example, the normal tissue could be labeled with a green dye and the tumor tissue with a red dye. These two samples are further hybridized on the same microarray chip. The relative intensities given by the red and green signals quantifies the level of difference between gene expression values across a normal-tumor pair.
\begin{figure}
\centering
\begin{tikzpicture}[node distance = 4cm, auto]
    \node[block] (tumor) {Tumor tissue};
    \node[block, right of=tumor,node distance = 5cm] (normal) {Normal tissue};
    
    \node[blockgreen, below of=normal] (green) {cDNA};
    \node[blockred, left of=green,node distance = 5cm] (red) {cDNA};
    \node[block, below of=green, xshift=-2.5cm] (stop) {Microarray Chip};
        
    \path[line] (tumor) -- node {mRNA extraction \& fluorescent Labeling }(red);
	\path[line] (normal) -- (green);
	\path[line] (green)  --(stop);
	\path[line] (red)  --  node{Hybridization} (stop);

\end{tikzpicture}
\label{Schema of a DNA microarray design}
\end{figure}

\subsection{Why Microarray?}
Studying which genes are active and which genes are silenced in a \textit{diseased} tissue versus the normal tissue can lead to a better understanding of the disease.
Instead of studying each gene individually, microarray technology is a high-throughput way \cite{sobek2006microarray} to study thousand of genes at once. This is a shift from the classical hypothesis based testing, where the biologist's focus would have been to investigate one gene ata a time. This \textit{omics} based high-throughput method involves study of thousands of genes at once, the hypothesis in most of the experiments remains the same: '\textit{Majority of genes are
not differentially expressed}' and as such the motivation is to identify those set of genes that are differentially expressed. A differentially expressed gene can either be over-expressed or under-expressed in a particular cell type, even though the genomic content does not vary between the cell types. The aim of  the study is thus to identify the smaller set of differentially expressed gene which could either be further studied for a biological point of view or can in turn be treated as a set of \textit{marker} to characterize the cell type, which can thus be used as diagnostic kit, thereby cutting down the need to profile from thousands of genes to a handful of them.

\section{Microarray: A data science problem}
There is an analogy between treating a microarray experiment to a classical clinical study. The traditional clinical study involved thousands to ten thousand cases with 
around 100 variables. A microarray study is just the transpose of this. Given the large number of variables involved , the system is largely \textit{undetermined}. Given this sort of high dimensionality with the number of variables far exceeding the number of observations and hence there is  a need to to get rid of this \textit{curse of dimensionality}


\begin{figure}
\centering{
	\begin{tikzpicture}	
	\draw[step=.1cm,gray,very thin] (-2,-2) grid (10,1);
	\draw[thick,->] (-2,-2) -- (10.5,-2) node[label={[xshift=-8.0cm, yshift=-1.0cm]Variables [10-100]},anchor=north west] {};
	\draw[thick,->] (-2,-2) -- (-2,2) node[label={[xshift=-1.2cm, yshift=-1.0cm]Cases [1000-10000]}, anchor=south east] {};	
	\end{tikzpicture}
	
	\label{microarray:history}
	\caption{Traditional clinical studies}
	}
	
\end{figure}

\begin{figure}
\centering{
	\begin{tikzpicture}
	
	\draw[step=.1cm,gray,very thin] (-2,-2) grid (2,7);
	\draw[thick,->] (-2,-2) -- (2,-2) node[label={[xshift=-3.0cm, yshift=-1.0cm]Variables [1000-10000]},anchor=north west] {};
	\draw[thick,->] (-2,-2) -- (-2,7) node[label={[xshift=-1.2cm, yshift=-1.0cm]Cases [10-100]}, anchor=south east] {};
	
	\end{tikzpicture}
	\label{microarray:current}
	\caption{ Curse of dimensionality with microarray and most other high throughput data}
	}
\end{figure}

\section{Data Analysis}

An output of a microarray experiment is a text file, (\textit{.gpr} in case of Genepix platform). This file stores spot intensities as ready by the scanner. A foreground is determined by the image processing software after the spots has been aligned by a gridding software. Due to the intermediate steps involved the color intensity recorded by the software is prone to noise and probably bias. Bias may arise due to various other factors:
\begin{itemize}
	\item Variation between chips arising due to manufacturing defects
	\item Amount of dye used might differ between various samples, hence leading to certain chips showing an overall high/low intensity
	\item In case of dual channel experiments, there have been reports of a gene-specific dye bias \cite{martin2005evaluation}
	\item Measurement errors, arising from the scanner being either sensitive to certain regions of the chip or having different sensitivity  for different intensity ranges
\end{itemize}

Most of these errors can be controlled via good a replication design. 

\subsection*{Replication}
Given that the experiments are always subject to random noise, mostly arising due to various environmental factors(for example room temperature might effect
 hybridization efficiency \cite{lu2008assessing}), replication can be a good tool to adjust for the associated variations. Replication is possible at two levels:\\
 \begin{itemize}
 	\item \textbf{Biological} : A biological replicate comes from a \textit{new} biological sample. The aim of a microarray experiment to define the set of differentially expressed genes between two or more cohorts and as such the interest lies in the average behavior of the genes across the two cohorts. These two cohorts should be representative of their respective population and hence requires that more members of the population be made part of the study as biological replicates. Even though there are chances of higher variation arising due to more subjects, the results will tend to be less biased.
 	\item \textbf{Technical}: Technical replication involves repeating the \textit{same} subject. So multiple \textit{samples} are created from the same subject. Though technical replicates can help reduce the variations involved due to random noise, the results however might be too specific for the cell line replicated and may not be true for the population.
 \end{itemize}
 
\subsection*{Microarray Experiments: Inherent assumptions and myths}
\subsubsection*{Myth: Gene expression values on  a chip follow Gaussian distribution}
Gene expression values on a single chip \textbf{do not} follow a Gaussian distribution. In fact given that microarray experiments can involve any subset of genes from the organism, it does not make sense to even assume that those genes will follow any kind of probability distribution.

A probability distributions assignment is to any process with repetitive measurements of the same instance of experiment. Hence though the gene expression values on a single chip experiment may define a \textit{distribution} but \textbf{not} a probability distribution.

It can however be assumed that the gene expression values follow a normal distribution \textit{across} arrays. This is an important assumption, which we encounter whilst performing parametric tests in order to identify the (statistically) differential expressed genes \cite{Giles2003}

Microarray data analysis involves several steps, which can broadly be divided into the following steps:
\begin{itemize}
\item \textbf{Exploratory Data Analysis and Quality Assessment}: This is often an essential step to determine which methods should be employed to pre-process the raw data and gauge the overall quality of data.
\item \textbf{{Background Correction}}: Involves separation of foreground signals from the background
\item \textbf{{Normalization}}: Adjustment for \textit{within-array} and \textit{between-array} bias that might arise due to different experimental conditions, dye-bias etc.

\end{itemize}

\section{Exploratory Data Analysis}
Before performing any sort of normalization it is often helpful to plot the raw intensities so that they can be compared post-processing.
The most often explored values are the foreground and background intensities. A lot of background signal in one of the samples might be an indicative of a faulty array.

%\begin{figure}[ht]
%	\begin{minipage}[b]{0.45\linewidth}
%	 	\centering
%		\includegraphics[width=\textwidth]{raw_foreground_grade2}
%		\caption{$log_2$ transformed foreground intensities of 17 control and 16 Grade II GBM patients}
%		\label{raw:foreground}
%	\end{minipage}	
%	\begin{minipage}[b]{0.45\linewidth}
%		\centering
%		\includegraphics[width=\textwidth]{raw_background_grade2}
%		\caption{$log_2$ transformed background intensities of 17 control and 16 Grade II GBM patients}
%		\label{raw:background}
%	\end{minipage}
%\end{figure}

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{raw_foreground_grade2}
	\caption{$log_2$ transformed foreground intensities of 17 control and 16 Grade II GBM patients}
	\label{raw:foreground}
\end{figure}

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{raw_background_grade2}
	\caption{$log_2$ transformed background intensities of 17 control and 16 Grade II GBM patients}
	\label{raw:background}
\end{figure}

\section{Background Correction}
The background intensities arise irrespective of true(foreground) intensities, often due to non specific binding of the dye to the spot, irrespective of the presence of the probe.
Background correction involves \textit{subtracting}  the background intensities from the foreground intensities. The background intensity can in fact not be measured at the spot directly and is read from the spots nearby.

\subsection{standard}
A n\"{a}ive approach to obtain the true signal intensity is to subtract the foreground intensity from the background intensity. This however can result into final intensities being negative.

An observed intensity can be modeled as an additive of two intensities, the true intensity $T$ and the residual background signal $B$, besides $S_b$, the additive background signal:\\
\begin{equation}
S = B + T + S_b
\end{equation}


\subsection{normexp method}
The normexp method \cite{shi2010optimizing} models the background intensity to be normally distributed:
\begin{equation}
B  \sim \mathcal{N} (m,\sigma^2)
\end{equation}

and the True signal is modelled as an exponential:\\

\begin{equation}
T  \sim \frac{1}{\alpha}  exp{\frac{-t}{\alpha}}
\end{equation}

The noise $B$ and true signal $T$ can be assumed to be independent random variables and hence the joint distribution of $B$ and $T$ is given by:
\begin{equation}
f_{B,T}(b,t;\mu,\sigma, \alpha) = \frac{1}{\alpha}  exp{\frac{-t}{\alpha}} \mathcal{N} (t;m,\sigma^2)
\end{equation}

Consider the random variable $X=S-S_b$ which is essentially the background subtracted intensity[foreground-background]. The joint distribution of X,S is given by \cite{Silver2009}:
\begin{equation}
f_{X,T}(x,t;\mu,\sigma, \alpha) = \frac{1}{\alpha}  exp(\frac{\sigma^2}{2\alpha^2}-\frac{x-\mu}{\sigma}) \mathcal{N} (t;\mu_{X,T},\sigma^2)
\end{equation}

\begin{equation}
\mu_{X,T}=x-\mu-\frac{\sigma^2}{\alpha}
\end{equation}

On integrating and dividing the joint by marginal: \\
\begin{equation}
f_{S|X}(s|x;\mu, \sigma, \alpha) = \frac{\mathcal{N} (t;\mu_{X,T},\sigma^2)}{1-\mathcal{N} (0; \mu_{X,T}, \sigma^2)}
\end{equation}

The estimated signal given the observed intensity $x$ is given by:\\
\begin{equation}
E(T|X=x) = \mu_{X,T} + \frac{\sigma^2\mathcal{N} (0; \mu_{X,T}, \sigma^2) }{1-\mathcal{N} (0; \mu_{X,T}, \sigma^2)}
\end{equation}
normexp produces positive intensities.

\subsection{normexp+offset}
normexp method can be stabilised for variance effects by shifting the baseline by adding a small offset to move the corrected intensities from zero.

\subsection{edwards}
Edwards method \cite{edwards2003non} by subtracting the background from the foreground only when the difference is larger than a threshold. Otherwise,  it is replaced by a smooth monotonic function

\subsection{rma}
The RMA(Robust Multi-Array Average) method works by partitioning the distribution of smoothed intensities around its mode.
The rma-75 and rma-mean estimators \cite{mcgee2006parameter} aim at correcting for the bias created by the rma algorithm.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{boxplot_BG_Correct_normexp_mle}
\caption{Boxplots after background correction using 'normexp+offset'. Offset=100}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{boxplot_quantile_normexp_mle}
\caption{Boxplots after 'quantile' normalisation of background corrected raw values using 'normexp+offset'}
\end{figure}


\section{Between Array Normalization}
Between array normalization is important if the box plots of the arrays are not consistent throughout width-wise.
We obtain the following boxplot after performing '\textit{normexp+offset}' background correction. The choice of  this particular background correction comes from the discussion given in Ritchie et al. \cite{ritchie2007comparison}.

 
\subsection*{MA plots}

For dual channel arrays, visualising the intensity difference between the Red and Green channel, would generally involve a scatter plot of the $R$ and $G$ channel intensities. The expected behaviour of this scatter plot is a diagonal. A slight modification of this strategy is to plot, $M$ and $A$:\\

\begin{equation}
M_i = log_2(\frac{R_i}{G_i})
\end{equation}

\begin{equation}
A_i = \frac{1}{2}log_2(R_iG_i)
\end{equation}
Thus plotting $M_i$ versus $A_i$ should in principle return a 45 degree rotated plot. Since the underlying hypothesis of a Microarray experiment is that \textit{majority} of genes are not differentially expressed, hence a MA-plot would involve just a single line  passing through x-axis, since the expected intensity of red and green channels are expected to bye similar. There are however points scattered around this line, depicting under and over expressed genes, potentially differentially expressed.

For a single channel the $R$ is analogous to the single channel intensity 	
\subsubsection{cyclicloess}

Loess is a \textit{non-parametric} methods used for smoothing scatter plots based on a locally weighted regression \cite{cleveland1996smoothing}. It involves fitting a polynomial to a subset of data at each point in the data set. The fitting in turn is based on weighted least squares. This involves a user specified input with $n$
the degree of polynomial to fit and a smoothing factor $\alpha$.  Values of $\alpha$ determines the proportion of dataset to be used for fitting the polynomial of degree $n$.

The polynomial fitting procedure gives most weight to the points lying closest to the point at which the polynomial estimation is taking place.

A polynomial $f(A_i)$ can be fit , using loess. The residuals are given by:

\begin{equation}
\bar{M_i} = M_i - f(A_i)
\end{equation} 

\begin{equation}
\bar{A_i}  = A_i
\end{equation}
These residuals will constitute a normalized MA plot.
In terms of $M_i$ and $A_i$:\\
\begin{equation}
log_2(R_i) = \bar{A_i} + \bar{M_i}/2
\end{equation} 

\begin{equation}
log_2(G_i)  = \bar{A_i} - \bar{M_i}/2
\end{equation}
For single channel arrays  the MA plots involve plotting  the different $M$, (equivalent to the background corrected intensity) versus a \textit{redefined} $A$, where $A$ can be taken to be average of all arrays, or be obtained from  a pairwise comparison everytime. The 'cyclic' in cyclicloess arises because this loess procedure is run over every possible pair of array chip comparisons.


\subsection{Quantile Normalization}
Quantile Normalization normalizes the value in two or more datasets by essentially making the distribution of the probe intensities identical statistically. This in turn is motivated by the concept of  \textbf{Q-Q plot}. A \textbf{Q-Q plot} is a graphical method to compare the values of two probability distributions. Given a case of plotting values of the reference dataset as the abscissa and those of the test dataset as the ordinate, the graph depict a diagonal line, given that the test and reference dataset  have identical probability distributions. This is simple to imagine since the highest value in the test dataset would correspond to the highest value in the reference dataset, and so on .

Quantile normalization thus makes the distribution of the test dataset identical to that of the reference dataset by associating the highest value in the test dataset to the highest value in the reference dataset, the next highest value in the test dataset to the second highest value in the reference dataset and so on. However there is no such \textit{reference} dataset in any microarray dataset. 

The problem of performing quantile normalization where there is no reference dataset can be tackled by going back to the motivation of \textbf{Q-Q plot}. Extending the concept of Q-Q plots to n-dimensions, if all the \textit{n} data vectors have identical distribution, a Q-Q plot would generate a straight line with its direction vector as:
\begin{center}


\begin{math}
d = (\frac{1}{\sqrt{n}},\frac{1}{\sqrt{n}},...,\frac{1}{\sqrt{n}} )
\end{math} 
\end{center}
Thus it is possible to make a set of data have identical distribution if it is projected along this diagonal \textit{d}.

Consider $k^{th}$ quantile data vector $q_k = (q_k1, q_k2, ..., q_kn) $ for $k = 1,2,3,...m$ for 
$m$ spots on $n$ arrays. A linear transformation of this vector given by:\\
\begin{center}


\begin{math}
	Q = (q_k.d).d 
\end{math}
\end{center}

%\centering
\begin{center}
\begin{math}
		Q = (\frac{1}{n} \sum\limits_{j=1}^n q_mj, ..., \frac{1}{n} \sum\limits_{j=1}^n q_mj)
\end{math}
\end{center}

Thus the reference dataset is created by extracting the mean quantile values from the given data vector. Typical steps to perform quantile normalization
for a given $m$ spots on $n$ arrays: \\
\begin{enumerate}
	\item Create a $m x n $ matrix say $X$
	\item Sort the columns in X so that the $1^{st}$ row has the highest values across all columns, $2^{nd}$ has the second highest values across all rows and so on.Call this array $X_sort$
	\item Calculate the row wise means of the sorted array $X_sort$
	\item Rearrange $X_sort$ replacing the rearranged values by the corresponding means calculated above.
\end{enumerate}

A short example demonstrating the approach:


\begin{table}
	\centering
	\begin{tabular}{c|c|c|c|}
		\hline 1 & 5 & 6 & 7  \\ 
		\hline 2 & 1 & 4 & 2 \\ 
		\hline 3 & 3 & 5 & 5 \\ 
		\hline 4 & 2 & 3 & 8 \\ 
		\hline 
	\end{tabular} 
	\caption{Original Data}
\end{table}

Sorted columns:\\

\begin{table}	
	\centering
	\begin{tabular}{c|c|c|c|}
		\hline 1 & 1 & 3 & 2  \\ 
		\hline 2 & 2 & 4 & 5 \\ 
		\hline 3 & 3 & 5 & 7 \\ 
		\hline 4 & 5 & 6 & 8 \\ 
		\hline 
	\end{tabular} 
	\caption{Data with sorted columns}
\end{table}


\begin{table}
		\centering
	\begin{tabular}{c|c|c|c|c}
		\hline 1 & 1 & 3 & 2 & 2.75 \\ 
		\hline 2 & 2 & 4 & 5 & 3.25\\ 
		\hline 3 & 3 & 5 & 7 & 4.5\\ 
		\hline 4 & 5 & 6 & 8 & 5.75\\ 
		\hline 
	\end{tabular} 
	\caption{Row means}
\end{table}

\begin{table}
		\centering
\begin{tabular}{|c|c|}
	\hline 1 & 2.75  \\ 
	\hline 2 & 3.25 \\ 
	\hline 3 & 4.5 \\ 
	\hline 4 & 5.75 \\ 
	\hline 
\end{tabular}
\caption{Rank Mappings }
\end{table}


\begin{table}
		\centering
	\begin{tabular}{c|c|c|c|}
		\hline 1 & 4 & 4 & 3  \\ 
		\hline 2 & 1 & 2 & 1 \\ 
		\hline 3 & 3 & 3 & 2 \\ 
		\hline 4 & 2 & 1 & 4 \\ 
		\hline 
	\end{tabular} 
	\caption{Rank Matrix:}
\end{table}


\begin{table}
		\centering
	\begin{tabular}{c|c|c|c|}
		\hline 2.75 & 5.75 & 5.75 & 4.5  \\ 
		\hline 3.25 & 2.75 & 3.25 & 2.75 \\ 
		\hline 4.5 & 4.5 & 4.5 & 3.25 \\ 
		\hline 5.75 & 3.25 & 2.75 & 5.75 \\ 
		\hline 
	\end{tabular} 
	\caption{Normalised Matrix}
\end{table}

\section{Differential Expression}

After the pre-processing, the next step is to identify the set of genes that are differentially expressed. This step essentially involves performing statistical tests
on the pre-precessed data and assigns rank to the genes based on them. Differentially expressed genes are the genes whose expression levels are outliers to standard state that the other genes exhibit.
The underlying hypothesis, to test of gene $i$ is differentially expressed  or not  is: \\
\begin{center}


$H_0$ = Gene $i$ is not differentially expressed \\
$H_1$ = Gene $i$ is differentially expressed \\
\end{center}
Test statistics are used to summarize the evidence in the data underlying $H_0$

\subsection{Fold Change}

Let $x_{i}^C$  and $x_{i}^D$ represent the $log2$ expression levels of the gene $i$  in Control and Disease sample respectively.
One definition of log Fold Change is given by \cite{Tusher2001}:\\
%\centering
\begin{center}
\begin{math}
	FC_{ratio} = \frac{\bar{x_i}^C}{\bar{x_i}^D}
\end{math}
\end{center}
where $\bar{x_i}^C$ and $\bar{x_i}^D$ represents the mean expression level of gene $i$ among Controls and Disease samples respectively.
However fold changes have also been calculated as : \cite{Guo2006} \\
%\centering
\begin{center}
\begin{math}
	FC_{difference} = \bar{x_i}^C - {\bar{x_i}^D}
\end{math}
\end{center}
Fold Change cut off is a naive approach used to determine \textit{differentially expressed} genes. The genes following a fold change above a certain threshold, generally between 1.8 and 3.0 are regarded as differentially expressed. Biologists tend to prefer this method of short-listing, even though  there are serious problems with this approach.  A fold change based cut off ignores the inherent variance that might exists in the short-listed genes, and considers only mean values of expression. Thus, genes with larger values of variance will often tend to be short-listed, just because of the noise involved. Other way round, the highly expressed genes will not be shortlisted because of the lower variability involved  and hence lower chances of showing  \cite{Claverie1999}. It is however been suggested that  
 the decision to use a fold change based cut off  is biological \cite{witten2007comparison}. The choice of a fold change can be justified if large absolute changes are indeed relevant to that particular experiment, ignoring the underlying noise.
 
 
\subsection{t test}
\subsubsection{Welch's t test }
The Welch's t-test uses the following statistic:\\
\begin{equation}
z_i = \frac{\bar{x_i}^C-\bar{x_i}^D}{s_i}
\end{equation}
where $s_i$ is the \textbf{non-pooled}  variance: \\

\begin{equation}
s_i = \sqrt{\frac{sc_i^2}{N_C} + \frac{sd_i^2}{N_D}}
\end{equation}

where $sc_i$  and $sd_2$ are the standard deviations with  sample sizes $N_C$ and $N_D$ for the control and disease respectively.
$z_i$ has degrees of freedom $df$ given by:\\
\begin{center}
\begin{math}
df = \frac{(a_c+a_d)^2}{\frac{a_c^2}{N_c-1} + \frac{a_d^2}{N_d-1}}
\end{math}
 \end{center}

where : \\
%\centering
\begin{center}
\begin{math}
	a_c = \frac{sc_i^2}{N_c}
\end{math}
\end{center}

This $z_i$ statistic follows a t-distribution: \\
%\centering
\begin{center}
\begin{math}
	z_i \sim t_i
\end{math}
\end{center}
the associated p-value is given by: \\
%\centering
\begin{center}


\begin{math}
	p-value  = 2*P(t_i \geq |z_i|)
\end{math}
\end{center}

\subsubsection{Pooled variance t-test}

For pooled variance t-test, the assumption is that the control and disease samples have the equal population variance and hence  $s_i$  is given by: \\
%\centering
\begin{center}


\begin{math}
s_i =  \sqrt{\frac{(N_C-1)sc_i^2+(N_D-1)sd_i^2}{N_C+N_D-2}}
\end{math}
\end{center}
and $z_i$ has $N_C+N_D-2$ degrees of freedom.
 \subsection{Linear Models for Microarray}
Smyth et al. \cite{Smyth2004a} suggested linear models for modeling microarray experiments. Consider $N$ set of samples in total with the gene $g$'s expression
values in the $n$ samples given by:\\
%\centering
\begin{center}


\begin{math}
	y_g^T = (y_{g1}, y_{g2}, ..., y_{gn})
\end{math}
\end{center}

$y_g^T$ contains the normalized $log2$ pre-processed intensities. Then,let the expectation of $y_g$be given by:\\
%\centering
\begin{center}


\begin{math}
E(y_g) = X\alpha_g
\end{math}
\end{center}

Where $X$ is the design matrix and $\alpha_g$ is an unknown coefficient vector. The variance of $y_g$ is given by: \\
%\centering
\begin{center}


\begin{math}	
	var(y_g) = W_g \sigma_g^2
\end{math}
\end{center}
where $W_g$ is a weight matrix, and $\sigma_g^2$ represents unknown gene wise variance. Consider $\beta_g$ as the log-fold change for gene $g$.
Instead of classical hypothesis testing where the test is $H_0:\beta_g=0$ versus $H_1=\beta_g\neq 0$, the test is conducted on thresholded values 
$H_0:|\beta_g|\leq \tau $  versus $H_1: |\beta_g| > \tau$, where $\tau$ is pre-specified log-fold change.


Assume the contrast to be tested is $\beta_g = c^T \alpha_g$ where $c^T$ is a contrast matrix like $X$. Since $\alpha_g$ is unknown,
given the response vectors and $X$ it is possible to fit a linear model to  obtain an estimate of coefficient vector as $\hat{\alpha_g}$  such that the covariance is given by: \\
%\centering
\begin{center}


\begin{math}
	var(\hat{\alpha_g}) = V_g\sigma_g^2 
\end{math}
\end{center}
 where $V_g$ is independent from $\sigma_g^2$ and is positive definite.
 
 Thus the estimate of $\beta_g$ is given by $\hat{\beta_g}=c^T\alpha_g$ 
 Assuming $\hat{\beta_g}$ to be normally distributed without forcing the normal distribution on $y_g$. $\hat{\beta_g}$ is assumed to be normally distributed with mean 
 $\beta_g$ and can be approximated as : \\
 \begin{center}


 \begin{math}
 	\hat{\beta_g}|\beta_g, \sigma_g^2 \sim \mathcal{N}(\beta_g, v_g\sigma^2)
 \end{math} \\
 \end{center}
 where 
 \begin{center}
 \begin{math}
 v_g = c^TV_gc
 \end{math}\\
 \end{center} 
 the variance $s_g^2$ is assumed to follow a scaled $\chi^2$ distribution.\\
 \begin{center}
 \begin{math}
 	s_g^2|\sigma_g^2 \sim \frac{\sigma_g^2}{d_g}\chi_{d_g}^2
\end{math}
\end{center}

where $d_g$ represents the residual degrees of freedom for gene $g$.

Under the above assumptions, the statistic $t_g$ follows a t-distribution with $d_g$ degrees of freedom: \\
\begin{center}
\begin{math}
	t_g = \frac{\hat{\beta_g}}{s_g \sqrt{v_g}}
\end{math}
 \end{center}
 
 \subsection{Correcting for multiple comparison}
 
 Determining the set of differentially expressed genes involves multiple hypothesis testing, where the 
 null hypothesis $H_0$ states that the gene is not differentially expressed. Thus for each gene, the test statistic tests  $H_0$,since these tests
 are performed on 1000 or more genes. This potentially leads to an increased chance of false positives.
 
 Considering a sample of 1000 genes with 30 of them differentially expressed. A $p-value$ is the probability of obtaining a result that is equal or more than the 
 actually observed given that the null hypothesis $H_0$ is true. The significance of a smaller $p-value$ lies in the fact that it implies that the observed values might be very rare given $H_0$ is true or $H_0$ is not true at all. Let the pre-defined threshold for the $p-value$ be $\alpha$ where we reject $H_0$ falsely with a probability of $\alpha$.
 
 With a $p$ value cut off of $0.05$, this would also imply of the remaining $1000-30=970$ non-differentially expressed genes,$0.05x970=49$ are false positives Thus the number off positives are greater than the truly expressed genes.
 
 In order to avoid this bias, arising due to over-fitting, the levels of significance can be adjusted for multiple testing. The \textit{Bonferroni} correction corrects this error by reducing the level of significance by a factor of $n$, the total samples.
 
 %Given the
% \subsection{Fox limma}
 
 
 \section{Materials and Methods}
 \subsection*{Datasets}
 Though DNA based microarrays are more common, here we tackle analysis of microarray from a proteomics study. 
 
 The data for the study was obtained from Glioblastoma multiforme(or GBM) patients. GBM is known to arise from the glial cells responsible for homeostasis. Homeostasis(biological homeostasis) is a property of the human body via which the body regulates the levels of certain variables in order to ensure that the internal metabolic reactions can be carried out in the right set of conditions. Regulation of blood's pH at a value of $7.365$ is thus a result of homeostasis.
 
 Clinicians, classify GBM into two broad categories:
 \begin{itemize}
 	\item Low Grade[Grade II]: Cells are non-anaplastic and the tumor is benign. The current methods of diagnosis reply MRI scans 
 	\item High Grade[Grade III and Grade IV] Cells are anaplastic and hence are dividing rapidly forming malignant tissues.
 \end{itemize}
 
 The data was collected from the following samples:
 \begin{enumerate}
 	\item \textbf{Controls}: A set of 17 control samples, collected from \textit{normal} patients. It is worth noting that the definition of \textbf{Controls} itself is a bit subjective. While comparing the gene expression levels with the \textit{disease} samples, we assume these set of controls to be \textbf{'healthy'}, and a representative sample of the \textit{healthy population}
 	\item \textbf{Grade II}: A set of 16 samples with Grade II glioma. 
 	\item \textbf{Grade III}: A set of 16 samples with Grade III glioma. 
 	\item \textbf{Grade IV}: A set of 16 samples with Grade IV glioma. 
 	
 \end{enumerate}
 
 The assignment of grades to the diseased samples was an outcome of the Clinician's analysis of the MRI and related tests. GenePix platform and software were used to 
 
 
 
 \subsection*{Defining the Question}
 Before performing any data analysis, it is very important to clearly define the question to be answered.
 The motivation behind performing this data analysis was:
 \begin{enumerate}
  \item Determine the set of differentially expressed genes in Grade II, Grade III and Grade IV samples with respect to the \textit{healthy} controls
  \item Compare the set of differentially expressed genes via a pairwise comparison between (Grade II, Grade III), (Grade III, Grade IV) and (Grade II, Grade IV)
  \item Determine a smaller panel of \textit{marker genes} that can potentially be used to differentiate the various Grades of GBM from control and possibly amongst themselves
 \end{enumerate}
 
 
 \subsection*{Exploratory Data Analysis}
 One of the key ideas, that needs to be taken care of is that the pre-processing steps should  be run on all the datasets  at once, rather than running them on any two cohorts taken together. This is important for any inferences that can be drawn out after the downstream analysis. 
 
 Consider the box plots for foreground and background intensities. Though a background intensity is always expected, \textit{subtraction} of this background intensity from the foreground should yield a similar looking boxplot across all arrays. However as evident from \ref{microarray:current}, this does not seem to be the case and this is the motivation to perform a normalization across all arrays 
 
 
 
 
 \subsection*{Normalization}
 The underlying hypothesis in a microarray study is that the expression levels of most of the genes are expected to be constant. Thus, a box plot with these expression values, should thus exhibit a behavior such that the mean expression levels across the samples remain the same. We employ 'quantile' based normalization here.
	
With normalization, the pre-processing work-flow is complete for the next downstream analysis for finding differentially expressed genes.
\begin{figure}
\includegraphics[width=\textwidth]{raw_foreground_negative}
\caption{Raw foreground log2 transformed intensities across negative control spots}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{raw_foreground_all}
\caption{Raw foreground log2 transformed intensities across all spots}
\end{figure}


\begin{figure}
\includegraphics[width=\textwidth]{foreground_normalized}
\caption{Foreground intensities post quantile normalization and background correction }
\end{figure}


\subsection*{Differentially expressed genes}

One of the good ways to visualize the set of differentially expressed genes is a Volcano plot. The plot plots log odd scores versus the  log fold change. Thus for a gen to be differentially expressed, just being above a certain fold change threshold is not sufficient s it must satisfy  must have  higher log odd score too.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{volcano_plot}
\caption{Volcano plot highlighting the statistically significant genes}
\end{figure}
	
\begin{figure}
\centering
\includegraphics[width=\textwidth]{top10_expressions}
\caption{Expression levels of the top 10 differentially expressed genes}
\end{figure}

\section{Discussion}
This chapter discusses the individual components of a pre-processing workflow, besides discussing the Mathematics, behind most of the methods. As a small case study, we present the list of differentially expressed genes in Grade-2 of GBM samples as compared to the Controls, after required pre-processing.

Though the list of differentially expressed  genes, the purpose of the problem defined in the beginning of this Chapter is not met yet. Had the question being asked just focussed on identifying those set of genes that show a marked behavior between Control and Disease samples, the list of differentially expressed gene would be a close answer. However we are also interested in finding if these expression profiles can be used to differentiate these cohorts. These set of differentially expressed genes could themselves be large in number. From the point of  biomarker design, a long list of these genes would not solve the process. The n\"{a}ive solution of selecting the first few differentially expressed genes, doe not work either because these top ranking genes might be part of  single pathway and hence a change in an upstream gene  will trigger the expression of the following  downstream genes in the pathway. Hence even though these genes are the top ranked in differential expression, they might collectively be unable to differentiate the control and disease samples, since the \textit{information} gain from such a list might be minimal.


The next chapters discuss the approach we take to potentially come up with such a bio-marker.
 
 
 