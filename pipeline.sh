if [ "$1" == "bowtie2" ];
then
    fasta="$bowtie_fasta"
    echo "####################################BOWTIE2-ALN-START################################################"
    echo "Starting alignment of $2 at `date`"
    starttime=`date +%s`
    echo `bowtie2 -x /data2/Amit_Dutt_Lab/human_g1k_v37_bowtie2/human_g1k_v37 -1 $2 -2 $3 -S aln.sam`
    endtime=`date +%s`
    echo "Ended alignment of $2 at `date`"
    ((diff_sec=endtime-starttime))
    echo "###TIME-TAKEN###"
    echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
    echo "###h:m:s###"
    echo "####################################BOWTIE2-ALN-END################################################"
fi
if [ "$1" == "bwa" ];
then
    fasta="$bwa_fasta"
    extension="${2##*.}"
    filename="${2%.*}"
    aln_file1="$filename.sai"
    echo "####################################BWA-ALN-START################################################"
    echo "Starting alignment of $aln_file1 at `date`"
    starttime=`date +%s`
    echo `bwa aln $fasta $2 > $aln_file1`
    endtime=`date +%s`
    echo "Ended alignment of $aln_file1 at `date`"
    ((diff_sec=endtime-starttime))
    echo "###TIME-TAKEN###"
    echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
    echo "###h:m:s###"
    echo "####################################BWA-ALN-END################################################"
   
    extension="${3##*.}"
    filename="${3%.*}"
    aln_file2="$filename.sai"
    echo "####################################BWA-ALN-START################################################"
    echo "Starting alignment of $3 at `date`"
    starttime=`date +%s`
    echo `bwa aln $fasta $3 > $aln_file2`
    endtime=`date +%s`
    echo "Ended alignment of $3 at `date`"
    ((diff_sec=endtime-starttime))
    echo "###TIME-TAKEN###"
    echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
    echo "###h:m:s###"
    echo "####################################BWA-ALN-END################################################"
    echo "####################################SAMFILE-GENERATATION-START################################################"
    echo "Starting alignment of $3 at `date`"
    starttime=`date +%s`
  #  bwa sampe -r "@rg\tid:@hwusi-eas100r:6:73:941:1973\tpl:illumina\tlb:lib-rdt\tsm:unknown\tpi:200" /data1/amit_dutt_lab/human_g1k_v37.fasta  $aln_file1 $aln_file2 $2 $3 > aln.sam
    bwa sampe /data1/Amit_Dutt_Lab/human_g1k_v37.fasta  $aln_file1 $aln_file2 $2 $3 > aln.sam
    endtime=`date +%s`
    echo "Ended alignment of $3 at `date`"
    ((diff_sec=endtime-starttime))
    echo "###TIME-TAKEN###"
    echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
    echo "###h:m:s###"
    echo "####################################SAMFILE-GENERATATION-END################################################"
fi
if [ "$1" == "soap" ];
then
    fasta="$soap_fasta"
    echo "####################################SOAP-ALN-START################################################"
    echo "Starting alignment of $2 and $3 at `date`"
    starttime=`date +%s`
    echo `soap -a $2 -b $3 -D $fasta -o soap_aln_paired.soap -2 soap_aln_single.soap `
    endtime=`date +%s`
    echo "Ended alignment of $2 and $3 at `date`"
    ((diff_sec=endtime-starttime))
    echo "###TIME-TAKEN###"
    echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
    echo "###h:m:s###"
    echo "####################################SOAP-ALN-END################################################"
    echo "####################################SAMFILE-GENERATATION-START################################################"
    echo "Converting SOAP to SAM at `date`"
    starttime=`date +%s`
    echo `perl /usr/bin/soap2sam.pl soap_aln_paired.soap > aln.sam`
    endtime=`date +%s`
    echo "Ended SOAP to SAM conversion at `date`"
    ((diff_sec=endtime-starttime))
    echo "###TIME-TAKEN###"
    echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
    echo "###h:m:s###"
    echo "####################################SAMFILE-GENERATATION-END################################################"
    
fi

echo "####################################ADD-READGROUPS-START################################################"
echo "Adding READ Groups data at `date`"
starttime=`date +%s`
echo `java -jar /usr/bin/picard-tools/AddOrReplaceReadGroups.jar INPUT=aln.sam RGLB=PAIRED_END RGPL=ILLUMINA RGPU=1 RGSM=ACTREC4 OUTPUT=aln.with.readgroups.sam`
endtime=`date +%s`
echo "Ended adding READ Groups at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################ADD-READGROUPS-END################################################"

echo "####################################FixMateInformation-START################################################"
echo "Fixing Mate Information at `date`"
starttime=`date +%s`
#echo `java -jar -Xmx20G /usr/bin/picard-tools/AddOrReplaceReadGroups.jar INPUT=aln.sam RGLB=PAIRED_END RGPL=ILLUMINA RGPU=1 RGSM=B2 OUTPUT=aln.with.readgroups.sam`
echo `java -jar /usr/bin/picard-tools/FixMateInformation.jar  INPUT=aln.with.readgroups.sam OUTPUT=aln.with.readgroups.fixed.sam`
endtime=`date +%s`
echo "Ending Fix Mate Information at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################FixMateInformation-END################################################"

echo "####################################SAMTOOLS-VALIDATION-START################################################"
echo "Adding READ Groups data at `date`"
starttime=`date +%s`
echo `java -jar /usr/bin/picard-tools/ValidateSamFile.jar I=aln.with.readgroups.fixed.sam O=validate_sam_file_results.txt`
endtime=`date +%s`
echo "Ended adding READ Groups at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################SAMTOOLS-VALIDATION-END################################################"


echo "####################################SAM-TO-BAM-START################################################"
echo "Converting SAM TO BAM at `date`"
starttime=`date +%s`
echo `samtools view -bS aln.with.readgroups.fixed.sam > aln.with.readgroups.bam`
endtime=`date +%s`
echo "Ended SAM TO BAM conversion at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################SAM-TO-BAM-END################################################"


echo "####################################BAQ-START################################################"
echo "Starting BAQ at `date`"
starttime=`date +%s`
#echo `samtools calmd -Abr aln.with.readgroups.bam > aln.with.readgroups.baq.bam`
mv aln.with.readgroups.bam aln.with.readgroups.baq.bam 
endtime=`date +%s`
echo "Ended BAQ conversion at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################BAQ-END################################################"


echo "####################################RMDUP-START################################################"
echo "Starting PCR removal at `date`"
starttime=`date +%s`
echo `samtools rmdup aln.with.readgroups.baq.bam aln.with.readgroups.baq.rmdup.bam`
endtime=`date +%s`
echo "Ended PCR removal at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################RMDUP-END################################################"

echo "####################################SAM-SORT-START################################################"
echo "Starting PCR removal at `date`"
starttime=`date +%s`
echo `samtools sort aln.with.readgroups.baq.rmdup.bam aln.with.readgroups.baq.rmdup.sorted`
endtime=`date +%s`
echo "Ended PCR removal at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################SAM-SORT-END################################################"

echo "####################################SAM-INDEX-START################################################"
echo "Starting indexing at `date`"
starttime=`date +%s`
echo `samtools index aln.with.readgroups.baq.rmdup.sorted.bam`
endtime=`date +%s`
echo "Ended indexing at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################SAM-INDEX-END################################################"


echo "####################################SAM-MPILEUP-START################################################"
echo "Starting mpileup at `date`"
starttime=`date +%s`
echo `samtools mpileup -uf $samtools_fasta aln.with.readgroups.baq.rmdup.sorted.bam | bcftools view -bvcg - > samtools_mpileup.raw.bcf `
endtime=`date +%s`
echo "Ended mpileup at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################SAM-MPILEUP-END################################################"

echo "####################################GATK-INDEL-START################################################"
echo "Starting indelaligner at `date`"
starttime=`date +%s`
echo `java -jar /usr/bin/GenomeAnalysisTK.jar -I aln.with.readgroups.baq.rmdup.sorted.bam -R /data1/Amit_Dutt_Lab/human_g1k_v37.fasta -T RealignerTargetCreator  -o forIndelRealigner.intervals  -et NO_ET -K /root/sandbox/saket.kumar_iitb.ac.in.key`
endtime=`date +%s`
echo "Ended IndelRealigner at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################GATK-INDEL-END################################################"

echo "####################################GATK-REALIGN-START################################################"
echo "Starting realigner at `date`"
starttime=`date +%s`
echo `java -jar /usr/bin/GenomeAnalysisTK.jar -I aln.with.readgroups.baq.rmdup.sorted.bam -R /data1/Amit_Dutt_Lab/human_g1k_v37.fasta -T IndelRealigner -targetIntervals forIndelRealigner.intervals -o realignedBam.bam  -et NO_ET -K /root/sandbox/saket.kumar_iitb.ac.in.key`
endtime=`date +%s`
echo "Ended Realigner at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################GATK-REALING-END################################################"

echo "####################################GATK-GENERATE-VCF-START################################################"
echo "Starting vcf generation at `date`"
starttime=`date +%s`
echo `java -jar /usr/bin/GenomeAnalysisTK.jar -l INFO -R /data1/Amit_Dutt_Lab/human_g1k_v37.fasta -T UnifiedGenotyper -I realignedBam.bam -o gatk_before_recalibration_vcf.vcf  -et NO_ET -K /root/sandbox/saket.kumar_iitb.ac.in.key`
endtime=`date +%s`
echo "Ended vcf genration at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################GATK-GENERATE-VCF-END################################################"
echo "####################################GATK-RECALIBRATION-START################################################"
echo "Starting recalibration at `date`"
starttime=`date +%s`
echo `java -Xmx4g -jar /usr/bin/GenomeAnalysisTK.jar -T BaseRecalibrator -I realignedBam.bam  -R /data1/Amit_Dutt_Lab/human_g1k_v37.fasta -knownSites /data1/Amit_Dutt_Lab/downloads/ftp.ncbi.nih.gov/snp/organisms/human_9606/VCF/00-All.vcf  -o recalibration_report.grp  -et NO_ET -K /root/sandbox/saket.kumar_iitb.ac.in.key`
endtime=`date +%s`
echo "Ended recalibration at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################GATK-RECALIBRATION-END################################################"

echo "####################################GATK-PRINTREADS-START################################################"
echo "Starting printreads at `date`"
starttime=`date +%s`
echo `java -jar /usr/bin/GenomeAnalysisTK.jar -T PrintReads  -R /data1/Amit_Dutt_Lab/human_g1k_v37.fasta -I realignedBam.bam -BQSR recalibration_report.grp -o recalibrated_bam.bam -et NO_ET -K /root/sandbox/saket.kumar_iitb.ac.in.key`
endtime=`date +%s`
echo "Ended printreads at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################GATK-PRINTREADS-END################################################"

echo "####################################GATK-GENOTYPER-START################################################"
echo "Starting genotyper at `date`"
starttime=`date +%s`
echo `java -jar /usr/bin/GenomeAnalysisTK.jar -l INFO -R /data1/Amit_Dutt_Lab/human_g1k_v37.fasta -T UnifiedGenotyper    -I recalibrated_bam.bam    -o gatk__after_calibration_vcf.vcf  -et NO_ET -K /root/sandbox/saket.kumar_iitb.ac.in.key`
endtime=`date +%s`
echo "Ended genotyper at `date`"
((diff_sec=endtime-starttime))
echo "###TIME-TAKEN###"
echo - | awk '{printf "%d:%d:%d","'"$diff_sec"'"/(60*60),"'"$diff_sec"'"%(60*60)/60,"'"$diff_sec"'"%60}'
echo "###h:m:s###"
echo "####################################GATK-GENOTYPER-END################################################"
