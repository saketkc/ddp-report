\select@language {english}
\contentsline {chapter}{Contents}{vii}{chapter*.1}
\contentsline {chapter}{List of Figures}{x}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}DNA Sequencing}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Sanger Sequencing}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Next Generation Sequencing}{5}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}NGS Data Formats}{8}{section.1.2}
\contentsline {section}{\numberline {1.3}Sequencing: Why?}{9}{section.1.3}
\contentsline {chapter}{\numberline {2}Driver mutation identification}{12}{chapter.2}
\contentsline {section}{\numberline {2.1}Driver Mutations}{12}{section.2.1}
\contentsline {section}{\numberline {2.2}Polyphen2 \cite {polyphen2}}{13}{section.2.2}
\contentsline {section}{\numberline {2.3}SIFT \cite {sift}}{14}{section.2.3}
\contentsline {section}{\numberline {2.4}Mutation Assessor \cite {mutationassesor}}{15}{section.2.4}
\contentsline {section}{\numberline {2.5}CHASM \cite {carter2009}}{17}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Feature Selection}{18}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}In silico mutations}{20}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Training and Output}{20}{subsection.2.5.3}
\contentsline {section}{\numberline {2.6}TransFIC \cite {gonzalez2012}}{20}{section.2.6}
\contentsline {chapter}{\numberline {3}Galaxy Toolboxes for Driver Mutation Discovery}{22}{chapter.3}
\contentsline {section}{\numberline {3.1}Galaxy}{22}{section.3.1}
\contentsline {section}{\numberline {3.2}Polyphen2}{23}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Input Format}{23}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Galaxy Workflow}{23}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}SIFT}{23}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Input Format}{25}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Galaxy Workflow}{25}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Mutation Assessor}{25}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Input Format}{25}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Galaxy Workflow}{26}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}TransFIC}{26}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Input Format}{26}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Galaxy Workflow}{27}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Condel}{27}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Input Format}{27}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Galaxy Workflow}{27}{subsection.3.6.2}
\contentsline {section}{\numberline {3.7}Results and Discussion}{27}{section.3.7}
\contentsline {chapter}{\numberline {4}Galaxy Visualisation Toolbox: A Case study}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}Data and Method}{29}{section.4.1}
\contentsline {chapter}{\numberline {5}Errors in Bioinformatics data analysis and Reproducible Research}{33}{chapter.5}
\contentsline {chapter}{\numberline {6}Detecting Viral Genomes in Cancer tumors}{37}{chapter.6}
\contentsline {chapter}{\numberline {7}Benchmarking BWA with BWA-PSSM}{41}{chapter.7}
\contentsline {chapter}{\numberline {8}Analysis of Microarray Data}{43}{chapter.8}
\contentsline {section}{\numberline {8.1}Introduction}{43}{section.8.1}
\contentsline {section}{\numberline {8.2}-Omics Research }{43}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Genomics}{44}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Proteomics}{44}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}Transcriptomics}{44}{subsection.8.2.3}
\contentsline {section}{\numberline {8.3}Microarray Technology}{44}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Motivation}{44}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Experimental Design}{45}{subsection.8.3.2}
\contentsline {subsection}{\numberline {8.3.3}Why Microarray?}{46}{subsection.8.3.3}
\contentsline {section}{\numberline {8.4}Microarray: A data science problem}{46}{section.8.4}
\contentsline {section}{\numberline {8.5}Data Analysis}{46}{section.8.5}
\contentsline {section}{\numberline {8.6}Exploratory Data Analysis}{49}{section.8.6}
\contentsline {section}{\numberline {8.7}Background Correction}{49}{section.8.7}
\contentsline {subsection}{\numberline {8.7.1}standard}{49}{subsection.8.7.1}
\contentsline {subsection}{\numberline {8.7.2}normexp method}{50}{subsection.8.7.2}
\contentsline {subsection}{\numberline {8.7.3}normexp+offset}{52}{subsection.8.7.3}
\contentsline {subsection}{\numberline {8.7.4}edwards}{52}{subsection.8.7.4}
\contentsline {subsection}{\numberline {8.7.5}rma}{52}{subsection.8.7.5}
\contentsline {section}{\numberline {8.8}Between Array Normalization}{52}{section.8.8}
\contentsline {subsubsection}{\numberline {8.8.0.1}cyclicloess}{53}{subsubsection.8.8.0.1}
\contentsline {subsection}{\numberline {8.8.1}Quantile Normalization}{54}{subsection.8.8.1}
\contentsline {section}{\numberline {8.9}Differential Expression}{55}{section.8.9}
\contentsline {subsection}{\numberline {8.9.1}Fold Change}{56}{subsection.8.9.1}
\contentsline {subsection}{\numberline {8.9.2}t test}{57}{subsection.8.9.2}
\contentsline {subsubsection}{\numberline {8.9.2.1}Welch's t test }{57}{subsubsection.8.9.2.1}
\contentsline {subsubsection}{\numberline {8.9.2.2}Pooled variance t-test}{58}{subsubsection.8.9.2.2}
\contentsline {subsection}{\numberline {8.9.3}Linear Models for Microarray}{58}{subsection.8.9.3}
\contentsline {subsection}{\numberline {8.9.4}Correcting for multiple comparison}{59}{subsection.8.9.4}
\contentsline {section}{\numberline {8.10}Materials and Methods}{60}{section.8.10}
\contentsline {section}{\numberline {8.11}Discussion}{62}{section.8.11}
\contentsline {chapter}{\numberline {9}Correspondence analaysis}{70}{chapter.9}
\contentsline {section}{\numberline {9.1}Introduction}{70}{section.9.1}
\contentsline {subsubsection}{\numberline {9.1.0.1}The significance of Chi-squared distance}{72}{subsubsection.9.1.0.1}
\contentsline {chapter}{\numberline {10}Classification of Microarray Data}{76}{chapter.10}
\contentsline {section}{\numberline {10.1}Curse of Dimensionality : Feature Selection}{77}{section.10.1}
\contentsline {section}{\numberline {10.2}SVM Classification}{77}{section.10.2}
\contentsline {section}{\numberline {10.3}Cross Validation and SVM}{79}{section.10.3}
\contentsline {section}{\numberline {10.4}Results and Discussions}{80}{section.10.4}
\contentsline {chapter}{\numberline {11}Visualisation tools for Bioinformatics }{83}{chapter.11}
\contentsline {section}{\numberline {11.1}Phred Score Viewer}{83}{section.11.1}
\contentsline {subsection}{\numberline {11.1.1}Implementation details}{83}{subsection.11.1.1}
\contentsline {section}{\numberline {11.2}Human Genetic Variation Viewer}{83}{section.11.2}
\contentsline {subsection}{\numberline {11.2.1}Implementation details}{84}{subsection.11.2.1}
\contentsline {subsection}{\numberline {11.2.2}Conclusion}{84}{subsection.11.2.2}
\contentsline {chapter}{\numberline {12}Conclusions}{86}{chapter.12}
\contentsline {chapter}{Appendix 1: Analysis of GBM Grade4 samples vs Control}{87}{appendix*.27}
\contentsline {chapter}{References}{94}{appendix*.28}
