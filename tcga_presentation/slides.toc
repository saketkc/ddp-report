\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{\scshape Introduction}{1}{0}{1}
\beamer@subsectionintoc {1}{1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{TCGA}{6}{0}{2}
\beamer@subsectionintoc {2}{1}{TCGA}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{Why TCGA}{18}{0}{2}
\beamer@sectionintoc {3}{Case Study}{22}{0}{3}
\beamer@subsectionintoc {3}{1}{Case Study I}{22}{0}{3}
\beamer@subsectionintoc {3}{2}{Case Study II}{24}{0}{3}
\beamer@subsectionintoc {3}{3}{Case Study III}{30}{0}{3}
\beamer@sectionintoc {4}{Wrapup}{36}{0}{4}
