\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{\scshape Introduction}{1}{0}{1}
\beamer@subsectionintoc {1}{1}{Objective}{3}{0}{1}
\beamer@sectionintoc {2}{\scshape Significant Mutations}{16}{0}{2}
\beamer@subsectionintoc {2}{1}{Motivation}{16}{0}{2}
\beamer@subsectionintoc {2}{2}{Next Generation Sequencing}{20}{0}{2}
\beamer@subsectionintoc {2}{3}{Computational Methods for Driver Detection}{23}{0}{2}
\beamer@sectionintoc {3}{\scshape Viral Genome Detecion}{37}{0}{3}
\beamer@subsectionintoc {3}{1}{Next Generation Sequencing }{37}{0}{3}
\beamer@sectionintoc {4}{\scshape Reproducibility}{44}{0}{4}
\beamer@subsectionintoc {4}{1}{Reproducibility}{44}{0}{4}
\beamer@sectionintoc {5}{\scshape Conclusions}{45}{0}{5}
\beamer@subsectionintoc {5}{1}{Wrapping up}{45}{0}{5}
