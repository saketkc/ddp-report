These are the LaTeX sources for the talk "Practical Affine Types,"
presented at POPL 2011. For more information:
http://www.eecs.harvard.edu/~tov/pubs/alms/

The slides require XeLaTeX to build (the version from TeX Live 2011
works for me), as well as the typefaces Monaco and Optima, which both
appear to come with Mac OS X. To build, running make once may be
insufficient, because some of the graphics require a few runs. I suggest
"make hard".

Jesse Tov
tov@eecs.harvard.edu
