\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{\scshape Introduction}{1}{0}{1}
\beamer@subsectionintoc {1}{1}{Objective}{2}{0}{1}
\beamer@sectionintoc {2}{Driver Mutation Detection}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Driver Mutation Detection}{9}{0}{2}
\beamer@sectionintoc {3}{Bio-marker prediction}{23}{0}{3}
\beamer@subsectionintoc {3}{1}{Introduction}{23}{0}{3}
\beamer@subsectionintoc {3}{2}{Preprocessing}{28}{0}{3}
\beamer@subsectionintoc {3}{3}{BG correction}{29}{0}{3}
\beamer@subsectionintoc {3}{4}{Normalization}{36}{0}{3}
\beamer@subsectionintoc {3}{5}{DE}{43}{0}{3}
\beamer@subsectionintoc {3}{6}{Correspondence Analysis}{47}{0}{3}
\beamer@subsectionintoc {3}{7}{Feature Extraction \& Classification}{69}{0}{3}
\beamer@sectionintoc {4}{Visualisation Tools}{95}{0}{4}
\beamer@subsectionintoc {4}{1}{Visualisation Tools}{95}{0}{4}
\beamer@subsectionintoc {4}{2}{Phred Score Viewer}{96}{0}{4}
\beamer@subsectionintoc {4}{3}{Variation Viewer}{101}{0}{4}
\beamer@sectionintoc {5}{Viral Genome Detection}{105}{0}{5}
\beamer@subsectionintoc {5}{1}{Viral Genome Detection}{105}{0}{5}
\beamer@sectionintoc {6}{BWA v/s BWA-PSSM}{111}{0}{6}
\beamer@subsectionintoc {6}{1}{BWA v/s BWA-PSSM}{111}{0}{6}
\beamer@sectionintoc {7}{Appendix}{121}{0}{7}
\beamer@subsectionintoc {7}{1}{Differential Expression Statistics}{122}{0}{7}
\beamer@subsectionintoc {7}{2}{Correspondence Analysis}{127}{0}{7}
\beamer@subsubsectionintoc {7}{2}{1}{The significance of Chi-squared distance}{127}{0}{7}
\beamer@subsectionintoc {7}{3}{SVM}{134}{0}{7}
